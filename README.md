
```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 博客作业_信用卡还款
{
    class CREposit
    {
        //定义还款人信息
        private string name;private double debet;private string paytime;
        public CREposit(string nam, double d, string p)
        {
            name = nam;debet = d;paytime = p;
        }
        public string get_name()
        { return name; }
        public double get_debet()
        { return debet; }
        public string get_paytime()
        { return paytime; }
    }
    class DEposit1
    {
        private double account;
        public void paydebet(CREposit d)
        {
            account = 5000;
            double de = d.get_debet();
            double re = account - de;
            if (re>= 0)
                Console.WriteLine("{0},储蓄卡1余额{1}元,本次还款{2}元,现余额{3}元,还款时间{4}", d.get_name(), account, de, re, d.get_paytime());
            else
                Console.WriteLine("储蓄卡1余额不足，请更换储蓄卡！");
        }
    }
    class DEposit2
    {
        private double account;
        public void paydebet(CREposit d)
        {
            account = 10000;
            double de = d.get_debet();
            double re = account - de;
            if ( re>= 0)
                Console.WriteLine("{0},储蓄卡2余额{1}元,本次还款{2}元,现余额{3}元,还款时间{4}", d.get_name(), account, de,re, d.get_paytime());
            else
                Console.WriteLine("储蓄卡2余额不足，请更换储蓄卡！");
        }
    }
    class DEposit3
    {
        private double account;
        public void paydebet(CREposit d)
        {
            account = 1000;
            double de = d.get_debet();
            double re = account - de;
            if (re >= 0)
                Console.WriteLine("{0},储蓄卡3余额{1}元,本次还款{3}元,现余额{4}元,还款时间{5}", d.get_name(), account, de,re, d.get_paytime());
            else
                Console.WriteLine("储蓄卡3余额不足，请更换储蓄卡！");
        }
    }
    class Delegate
    {
        //定义委托
        public delegate void MeDelegate(CREposit d);
        //定义事件
        public event MeDelegate SubMoney;

        public void Sub(CREposit d)
        {
            //如果事件不为空
            if (SubMoney != null)
            {
                Console.WriteLine("进入还款流程");
                SubMoney(d);
            }
            else
            {
                Console.WriteLine("还款日期为{0}", d.get_paytime());
                Console.WriteLine("今天是{0},未到还款时间", DateTime.Now.ToString("yyyy/MM/dd"));
            }
        }
    }

    class program
    {
        static void Main(string[] args)
        {
            Delegate m1 = new Delegate();
            CREposit d1 = new CREposit("DUAN", 6000, "2022/04/01");
            DEposit1 c1 = new DEposit1();
            DEposit2 c2 = new DEposit2();
            DEposit3 c3 = new DEposit3();
            //string Nowday = Console.ReadLine();
            if (d1.get_paytime() == DateTime.Now.ToString("yyyy/MM/dd"))
            {
                Console.WriteLine("请输入你选择的储蓄卡：");
                string Card = Console.ReadLine();
                switch (Card)
                {
                    case "DEposit1": m1.SubMoney += c1.paydebet; break;
                    case "DEposit2": m1.SubMoney += c2.paydebet; break;
                    case "DEposit3": m1.SubMoney += c3.paydebet; break;
                }
            }
            m1.Sub(d1);
        }
    }
}
```
